package com.photon.chasenyctest.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.photon.chasenyctest.databinding.SchoolViewBinding
import com.photon.chasenyctest.model.School

typealias SchoolListener = (School) -> Unit

class SchoolAdapter(
    private val onSchoolClick: SchoolListener
) : ListAdapter<School, SchoolAdapter.SchoolViewHolder>(SchoolDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        return SchoolViewHolder(
            SchoolViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class SchoolDiff() : DiffUtil.ItemCallback<School>() {
        override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem.dbn == newItem.dbn
        }

        override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem == newItem
        }
    }

    inner class SchoolViewHolder(private val binding: SchoolViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(school: School) {
            with(binding) {
                schoolDetails.setOnClickListener { onSchoolClick(school) }
                schoolDbn.text = school.dbn
                schoolName.text = school.name
                schoolLocation.text = school.location
                schoolEmail.text = school.email
                schoolPhone.text = school.phone
            }
        }

    }
}