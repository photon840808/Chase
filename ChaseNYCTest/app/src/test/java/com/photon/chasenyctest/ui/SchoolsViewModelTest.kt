package com.photon.chasenyctest.ui.schools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.photon.chasenyctest.alliance.repository.SchoolsRepository
import com.photon.chasenyctest.model.School
import com.photon.chasenyctest.model.SchoolSat
import com.photon.chasenyctest.utils.Resource
import com.photon.chasenyctest.utils.getOrAwaitValue
import com.photon.chasenyctest.viewmodel.SchoolsViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Rule

import org.junit.Test

class SchoolsViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val repository: SchoolsRepository = mock()
    private val dispatcherIO: CoroutineDispatcher = Dispatchers.IO
    private val defaultDispatcher: CoroutineDispatcher = Dispatchers.Default
    private val viewModel by lazy {
        SchoolsViewModel(
            repository = repository,
            dispatcherIo = dispatcherIO,
            defaultDispatcher = defaultDispatcher
        )
    }

    @Test
    fun refreshSchools() {
        runBlocking {
            val school = School("12345",
                "School",
                "2248041653",
                "Chicago, IL",
                "test@photon.com")
            val resource = Resource.Success(listOf(school))
            //Given
            whenever(repository.getSchools()).thenReturn(resource)
            //When
            viewModel.refreshSchools()

            //Then
            val result = viewModel.schools.getOrAwaitValue()
            assertThat(result).hasSize(1)
            val first = result.first()
            assertThat(first.dbn).isEqualTo("12345")
            assertThat(first.name).isEqualTo("School")
            assertThat(first.phone).isEqualTo("2248041653")
            assertThat(first.location).isEqualTo("Chicago, IL")
            assertThat(first.email).isEqualTo("test@photon.com")
        }
    }

    @Test
    fun getSchoolDetails() {
        runBlocking {
            //Given
            val school = School("12345",
                "School",
                "2248041653",
                "Chicago, IL",
                "test@photon.com")
            val schoolSat = SchoolSat("12345",
                10,
                9.1,
                8.1,
                7.1)
            val resource = Resource.Success(listOf(schoolSat))
            whenever(repository.getSatSchools()).thenReturn(resource)

            //When
            viewModel.getSchoolDetails(school)

            //Then
            val result = viewModel.details.getOrAwaitValue()
            val resultSchool = result.school
            val resultSat = result.sat

            assertThat(resultSchool.dbn).isEqualTo("12345")
            assertThat(resultSchool.name).isEqualTo("School")
            assertThat(resultSchool.phone).isEqualTo("2248041653")
            assertThat(resultSchool.location).isEqualTo("Chicago, IL")
            assertThat(resultSchool.email).isEqualTo("test@photon.com")

            assertThat(resultSat.dbn).isEqualTo("12345")
            assertThat(resultSat.students).isEqualTo(10)
            assertThat(resultSat.avgMath).isEqualTo(9.1)
            assertThat(resultSat.avgReading).isEqualTo(8.1)
            assertThat(resultSat.avgWriting).isEqualTo(7.1)
        }
    }
}