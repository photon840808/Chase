package com.photon.chasenyctest.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.photon.chasenyctest.R
import com.photon.chasenyctest.databinding.ActivityDetailsBinding
import com.photon.chasenyctest.model.SchoolDetails

class DetailActivity: AppCompatActivity() {

    private lateinit var detailBinding: ActivityDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        detailBinding = ActivityDetailsBinding.inflate(layoutInflater)
        setContentView(detailBinding.root)

        intent.extras?.getParcelable<SchoolDetails>(EXTRA_DETAILS)?.let {
            updateView(it)
        }
    }

    private fun updateView(details: SchoolDetails) {
        with(detailBinding){
            schoolDetailsDbn.text = details.school.dbn
            schoolDetailsName.text = details.school.name
            schoolDetailsStudents.text = getString(R.string.num_of_sat_test_takers, details.sat.students)
            schoolDetailsMath.text = getString(R.string.sat_math_avg_score, details.sat.avgMath)
            schoolDetailsReading.text = getString(R.string.sat_critical_reading_avg_score, details.sat.avgReading)
            schoolDetailsWriting.text = getString(R.string.sat_writing_avg_score, details.sat.avgWriting)
        }
    }


    companion object {
        fun intent(ctx: Context, schoolDetails: SchoolDetails) = Intent(ctx, DetailActivity::class.java).apply {
            putExtra(EXTRA_DETAILS, schoolDetails)
        }

        const val EXTRA_DETAILS = "EXTRA_DETAILS"
    }
}