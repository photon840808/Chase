package com.photon.chasenyctest.domain

import com.photon.chasenyctest.model.School
import com.photon.chasenyctest.model.SchoolSat
import com.photon.chasenyctest.model.SchoolSatResponse
import com.photon.chasenyctest.model.SchoolsResponse

fun SchoolsResponse.toDomain() = School(
    dbn = dbn?:"",
    name = school_name?:"",
    phone = phone_number?:"",
    location = "${city}, $state_code",
    email = school_email?:""
)

fun SchoolSatResponse.toDomain() = SchoolSat(
    dbn = dbn?:"",
    avgMath = sat_math_avg_score?.toDoubleOrNull()?: 0.0,
    avgReading = sat_critical_reading_avg_score?.toDoubleOrNull()?: 0.0,
    avgWriting = sat_writing_avg_score?.toDoubleOrNull()?: 0.0,
    students = num_of_sat_test_takers?.toIntOrNull()?: -1
)