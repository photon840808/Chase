package com.photon.chasenyctest.utils

sealed class Resource<T>{
    data class Success<T>(val data: T): Resource<T>()
    data class Failure<T>(val data: T?, val error: Throwable): Resource<T>()
}
