package com.photon.chasenyctest.data

import com.photon.chasenyctest.model.SchoolSatResponse
import com.photon.chasenyctest.model.SchoolsResponse
import com.photon.chasenyctest.alliance.network.SchoolsAPI
import retrofit2.Response
import javax.inject.Inject

class NetworkSchoolSource @Inject constructor(
    private val schoolsAPI: SchoolsAPI
) {
    suspend fun getSchools(): Response<List<SchoolsResponse>> {
        return schoolsAPI.getSchoolsList()
    }

    suspend fun getSchoolSat(): Response<List<SchoolSatResponse>> {
        return schoolsAPI.getSchoolSatList()
    }

    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"
    }
}