package com.photon.chasenyctest.domain

import com.google.common.truth.Truth.assertThat
import com.photon.chasenyctest.model.SchoolSatResponse
import com.photon.chasenyctest.model.SchoolsResponse

import org.junit.Test

class SchoolsMappersTest {

    @Test
    fun `SchoolsResponse to School mapper`() {
        //Given
        val responseFake = SchoolsResponse(
            dbn = "12345",
            school_name = "My School",
            phone_number = "2248041653",
            city = "Chicago",
            state_code = "IL",
            school_email = "test@photon.com"
        )

        //When
        val result = responseFake.toDomain()

        //Then
        assertThat(result.dbn).isEqualTo("12345")
        assertThat(result.name).isEqualTo("My School")
        assertThat(result.phone).isEqualTo("2248041653")
        assertThat(result.location).isEqualTo("Chicago, IL")
        assertThat(result.email).isEqualTo("test@photon.com")
    }

    @Test
    fun `SchoolSatResponse to SchoolSat mapper`() {
        //Given
        val responseFake = SchoolSatResponse(
            dbn = "12345",
            school_name = "My School",
            num_of_sat_test_takers = "10",
            sat_math_avg_score = "9.1",
            sat_critical_reading_avg_score = "8.1",
            sat_writing_avg_score = "7.1"
        )

        //When
        val result = responseFake.toDomain()

        //Then
        assertThat(result.dbn).isEqualTo("12345")
        assertThat(result.students).isEqualTo(10)
        assertThat(result.avgMath).isEqualTo(9.1)
        assertThat(result.avgReading).isEqualTo(8.1)
        assertThat(result.avgWriting).isEqualTo(7.1)
    }
}