package com.photon.chasenyctest.module

import com.photon.chasenyctest.data.NetworkSchoolSource
import com.photon.chasenyctest.data.SchoolsRepositoryImpl
import com.photon.chasenyctest.alliance.repository.SchoolsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Singleton
    @Provides
    fun SchoolsRepository(dataSource: NetworkSchoolSource): SchoolsRepository = SchoolsRepositoryImpl(dataSource)
}