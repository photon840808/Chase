package com.photon.chasenyctest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import com.photon.chasenyctest.viewmodel.SchoolsViewModel
import com.photon.chasenyctest.adapter.SchoolAdapter
import com.photon.chasenyctest.databinding.ActivitySchoolBinding
import com.photon.chasenyctest.model.School
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolsActivity : AppCompatActivity() {
    private lateinit var schoolBinding: ActivitySchoolBinding
    private val adapter by lazy { SchoolAdapter(::onSchoolClicked) }
    private val viewModel by viewModels<SchoolsViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        schoolBinding = ActivitySchoolBinding.inflate(layoutInflater)
        setContentView(schoolBinding.root)

        setupView()
        setupObservers()
        viewModel.refreshSchools()
    }

    private fun setupObservers() {
        viewModel.schools.observe(this){
            updateSchoolsList(it)
        }
        viewModel.details.observe(this){ details ->
            startActivity(DetailActivity.intent(this, details))
        }
        viewModel.errorMessage.observe(this){error ->
            Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setupView() {
        with(schoolBinding){
            schoolsRecycler.adapter = adapter
        }
    }

    private fun updateSchoolsList(schools: List<School>){
        adapter.submitList(schools)
    }

    private fun onSchoolClicked(school: School){
        viewModel.getSchoolDetails(school)
    }
}


