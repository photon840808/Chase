package com.photon.chasenyctest.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SchoolDetails(
    val school: School,
    val sat: SchoolSat
): Parcelable
