package com.photon.chasenyctest.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SchoolSat(
    val dbn: String = "",
    val students: Int = -1,
    val avgMath: Double = 0.0,
    val avgReading: Double = 0.0,
    val avgWriting: Double = 0.0
): Parcelable
