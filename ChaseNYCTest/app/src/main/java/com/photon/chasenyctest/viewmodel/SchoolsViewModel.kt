package com.photon.chasenyctest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.photon.chasenyctest.alliance.repository.SchoolsRepository
import com.photon.chasenyctest.model.School
import com.photon.chasenyctest.model.SchoolDetails
import com.photon.chasenyctest.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class SchoolsViewModel @Inject constructor(
    private val repository: SchoolsRepository,
    @Named("IO") private val dispatcherIo: CoroutineDispatcher,
    @Named("Default") private val defaultDispatcher: CoroutineDispatcher
) : ViewModel() {
    private val _schools = MutableLiveData<List<School>>()
    val schools: LiveData<List<School>> get() = _schools

    private val _details = MutableLiveData<SchoolDetails>()
    val details: LiveData<SchoolDetails> get() = _details

    private val _errorMessage = MutableLiveData<String?>()
    val errorMessage: LiveData<String?> get() = _errorMessage

    fun refreshSchools() {
        viewModelScope.launch(dispatcherIo) {
            val result = repository.getSchools()
            when(result){
                is Resource.Failure -> {
                    _errorMessage.postValue(result.error.message)
                }
                is Resource.Success -> {
                    _schools.postValue(result.data ?: null)
                }
            }

        }
    }

    fun getSchoolDetails(school: School) {
        viewModelScope.launch(dispatcherIo) {
            val result = repository.getSatSchools()
            withContext(defaultDispatcher) {

                when(result){
                    is Resource.Failure -> {
                        _errorMessage.postValue(result.error.message)
                    }
                    is Resource.Success -> {
                        result.data.find { it.dbn == school.dbn }.let { schoolSat ->
                            if(schoolSat != null) {
                                _details.postValue(SchoolDetails(school, schoolSat))
                            } else {
                                _errorMessage.postValue("Details not found")
                            }
                        }
                    }
                }
            }
        }
    }

}