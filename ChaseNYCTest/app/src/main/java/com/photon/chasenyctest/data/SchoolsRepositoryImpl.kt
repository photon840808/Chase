package com.photon.chasenyctest.data

import com.photon.chasenyctest.utils.Resource
import com.photon.chasenyctest.alliance.repository.SchoolsRepository
import com.photon.chasenyctest.domain.toDomain
import com.photon.chasenyctest.model.School
import com.photon.chasenyctest.model.SchoolSat

class SchoolsRepositoryImpl(
    private val remoteSchoolsDataSource: NetworkSchoolSource
) : SchoolsRepository {

    override suspend fun getSchools(): Resource<List<School>> {
        val result = remoteSchoolsDataSource.getSchools()
        return when(result.isSuccessful){
            true -> Resource.Success(result.body()?.map { it.toDomain() } ?: emptyList())
            false -> Resource.Failure(null, Exception(result.message()))
        }
    }

    override suspend fun getSatSchools(): Resource<List<SchoolSat>> {
        val result = remoteSchoolsDataSource.getSchoolSat()
        return when(result.isSuccessful){
            true -> Resource.Success(result.body()?.map { response -> response.toDomain() } ?: emptyList())
            false -> Resource.Failure(null, Exception(result.message()))
        }
    }
}