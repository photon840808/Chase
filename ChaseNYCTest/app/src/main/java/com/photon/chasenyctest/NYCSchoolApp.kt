package com.photon.chasenyctest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolApp: Application() {
}