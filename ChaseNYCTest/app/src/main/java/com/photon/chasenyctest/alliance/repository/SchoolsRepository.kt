package com.photon.chasenyctest.alliance.repository

import com.photon.chasenyctest.model.School
import com.photon.chasenyctest.model.SchoolSat
import com.photon.chasenyctest.utils.Resource

interface SchoolsRepository {
    suspend fun getSchools(): Resource<List<School>>
    suspend fun getSatSchools(): Resource<List<SchoolSat>>
}