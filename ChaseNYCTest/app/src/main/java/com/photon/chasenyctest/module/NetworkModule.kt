package com.photon.chasenyctest.module

import com.photon.chasenyctest.BuildConfig
import com.photon.chasenyctest.data.NetworkSchoolSource
import com.photon.chasenyctest.alliance.network.SchoolsAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Singleton
    @Provides
    fun providesOkHttpClient(): OkHttpClient = OkHttpClient.Builder().addInterceptor(
        HttpLoggingInterceptor().apply {
            level = if(BuildConfig.DEBUG){
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        }
    ).build()

    @Singleton
    @Provides
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder().client(okHttpClient).baseUrl(
        NetworkSchoolSource.BASE_URL
    ).addConverterFactory(GsonConverterFactory.create()).build()

    @Singleton
    @Provides
    fun providesSchoolAPI(retrofit: Retrofit): SchoolsAPI = retrofit.create(SchoolsAPI::class.java)

    @Singleton
    @Provides
    fun providesNetworkDatasource(schoolsAPI: SchoolsAPI): NetworkSchoolSource = NetworkSchoolSource(schoolsAPI)
}


