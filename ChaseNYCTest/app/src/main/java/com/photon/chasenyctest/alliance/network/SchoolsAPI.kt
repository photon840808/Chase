package com.photon.chasenyctest.alliance.network

import com.photon.chasenyctest.model.SchoolSatResponse
import com.photon.chasenyctest.model.SchoolsResponse
import retrofit2.Response
import retrofit2.http.GET

interface SchoolsAPI {
    @GET("s3k6-pzi2.json")
    suspend fun getSchoolsList(): Response<List<SchoolsResponse>>

    @GET("f9bf-2cp4.json")
    suspend fun getSchoolSatList(): Response<List<SchoolSatResponse>>
}