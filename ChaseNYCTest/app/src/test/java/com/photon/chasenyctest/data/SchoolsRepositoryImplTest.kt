package com.photon.chasenyctest.data

import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.photon.chasenyctest.model.SchoolSatResponse
import com.photon.chasenyctest.model.SchoolsResponse
import com.photon.chasenyctest.utils.Resource
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody.Companion.toResponseBody

import org.junit.Test
import retrofit2.Response

class SchoolsRepositoryImplTest {

    private val dataSource: NetworkSchoolSource = mock()
    private val repository by lazy { SchoolsRepositoryImpl(dataSource) }

    @Test
    fun `getSchools - Given network failure then repository return Resource Success`() {
        runBlocking {
            //Given
            val schoolsSat = listOf<SchoolsResponse>(mock(), mock())
            val response = Response.success(schoolsSat)
            whenever(dataSource.getSchools()).thenReturn(response)

            //When
            val resource = repository.getSchools()

            //Then
            assertThat(resource).isInstanceOf(Resource.Success::class.java)
            resource as Resource.Success
            assertThat(resource.data).hasSize(2)
        }
    }

    @Test
    fun `getSchools - Given network failure then repository return Resource Failure`() {
        runBlocking {
            //Given
            val response = Response.error<List<SchoolsResponse>>(500, "".toResponseBody())
            whenever(dataSource.getSchools()).thenReturn(response)

            //When
            val resource = repository.getSchools()

            //Then
            assertThat(resource).isInstanceOf(Resource.Failure::class.java)
            resource as Resource.Failure
            assertThat(resource.data).isNull()
        }
    }

    @Test
    fun `getSchoolSat - Given network failure then repository return Resource Success`() {
        runBlocking {
            //Given
            val schoolsSat = listOf<SchoolSatResponse>(mock(), mock())
            val response = Response.success(schoolsSat)
            whenever(dataSource.getSchoolSat()).thenReturn(response)

            //When
            val resource = repository.getSatSchools()

            //Then
            assertThat(resource).isInstanceOf(Resource.Success::class.java)
            resource as Resource.Success
            assertThat(resource.data).hasSize(2)
        }
    }

    @Test
    fun `getSchoolSat - Given network failure then repository return Resource Failure`() {
        runBlocking {
            //Given
            val response = Response.error<List<SchoolSatResponse>>(500, "".toResponseBody())
            whenever(dataSource.getSchoolSat()).thenReturn(response)

            //When
            val resource = repository.getSatSchools()

            //Then
            assertThat(resource).isInstanceOf(Resource.Failure::class.java)
            resource as Resource.Failure
            assertThat(resource.data).isNull()
        }
    }
}